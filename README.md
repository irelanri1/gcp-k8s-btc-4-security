# gke-k8s-btc-4-security

Creates a Google (IAM) Service Account, and binds this to a pre-existing Kubernetes Service Account, such as would be createed by the previous project.

This completes the arrangement whereby the K8S Service Account (assigned to the service) can assume privileges of the Google Service Account for Google API usage.

See [this article](https://cloud.google.com/kubernetes-engine/docs/how-to/workload-identity#config-connector) for a more detailed explanation.





