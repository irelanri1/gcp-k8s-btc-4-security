# Google Service Account ...

resource "google_service_account" "gcp-btc-service-account" {
  account_id   = "gcp-btc-service-account"
  display_name = "BTC Service Account"
}

# Which we then map to our K8S Service Account, so that our K8S Service Account
# can assume this identity for using Google APIs

resource "google_service_account_iam_binding" "btc-service-binding" {
  service_account_id = google_service_account.gcp-btc-service-account.name
  role               = "roles/iam.serviceAccountUser"

  members = [
    "serviceAccount:${var.project_id}.svc.id.goog[btc/btc-service-account]",
  ]
}